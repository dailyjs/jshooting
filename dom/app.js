
/**
 * Module dependencies.
 */

var express = require('express');
var routes = require('./routes');
var test = require('./routes/test');
var http = require('http');
var path = require('path');
var socket = require('socket.io');
var util = require('util');

var app = express();

// all environments
app.set('port', 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);
app.get('/test', test.toy);

var server = http.createServer(app);
server.listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});

function randInt(max) {
  return parseInt(Math.random() * max);
}


var io = socket.listen(server);
io.set('log level', 2);
var idSoFar = 0;
var userData = {};
var socketIdToUserIdMap = {};
var noConnections = 0;
var noPlaying = 0;
var world = {
  canvasWidth: 500,
  canvasHeight: 600,
  barometerWidth: 400,
  barometerHeight: 600
};

function startServerBullets() {
  var targetBullet = setInterval(bulletCollection.TargetBullet, 500);
  var circleBullet = setInterval(bulletCollection.OctetCircleBullet, 2000);
  bulletIntervalList.push(targetBullet);
  bulletIntervalList.push(circleBullet);
}
function endServerBullets() {
  for (var i = 0; i < bulletIntervalList.length; i++) {
    clearInterval(bulletIntervalList[i]);
  }
  bulletIntervalList = [];
}

io.sockets.on('connection', function(socket) {

  noConnections += 1;

  socket.emit('welcome world', world);
  socket.on('world complete', function() {
    var newUserData = {
      id: idSoFar++,
      color: util.format('rgb(%d,%d,%d)', randInt(255), randInt(255), randInt(255)),
      x: 200,
      y: 200,
      direction: [false, false, false, false],
      playing: true // for server check
    };
    socket.emit('welcome user', newUserData, userData);
    userData[newUserData.id] = newUserData;
    socketIdToUserIdMap[socket.id] = newUserData.id;
    console.log('Current noConnection: ', noConnections);
  });
  socket.on('request start', function(userID) {
    noPlaying += 1;
    if (noPlaying === 1) {
      startServerBullets();
    }
    userData[userID].direction = [false, false, false, false];
    socket.broadcast.emit('user connected', userData[userID]);
    socket.emit('start game');
  });
  socket.on('end game', function(userID) {
    socket.broadcast.emit('user end', userData[userID]);
    noPlaying -=1;
    console.log('End user: ', userID);
    userData[userID].playing = false;
    if (noPlaying === 0) {
      endServerBullets();
    }
  });

  socket.on('disconnect', function() {
    var disconnectedUserId = socketIdToUserIdMap[socket.id];
    console.log('disconnect', disconnectedUserId);
    socket.broadcast.emit('user disconnected', disconnectedUserId, userData);
    noConnections -= 1;
    if (userData[disconnectedUserId].playing === true) {
      noPlaying -= 1;
      if (noPlaying === 0) {
        endServerBullets();
      }
    }
    delete userData[disconnectedUserId];
    delete socketIdToUserIdMap[socket.id];

    console.log('The number of users currently connected: ', noConnections);
  });
  socket.on('user move start', function(user) {
    socket.broadcast.emit('user move start', user);

  });
  socket.on('user move end', function(user) {
    socket.broadcast.emit('user move end', user);
    var u = userData[user.id];
    u.x = user.posX;
    u.y = user.posY;
    u.direction[user.keyCode - 37] = false;
    u.timestamp = user.timestamp;
  });
  socket.on('ping', function(t) {
    var now = (new Date()).getTime();
    var cts = now - t.timestamp;
    socket.emit('pong', {
      ct: t.timestamp,
      cts: cts,
      st: now
    });
  });
});


var bulletIntervalList = [];
var bulletCollection = {
  TargetBullet: function() {
    if (noConnections === 0) {
      return;
    }
    var source = generateRandomWallBulletLocation();
    var bullet = {
      sourceX: source.left,
      sourceY: source.top,
      targetId: userData[parseInt((Math.random() * 10000) % Object.keys(userData).length)].id,
      speed: 0.2,
      timestamp: (new Date).getTime()
    };
    console.log('Create target bullet: ', bullet.timestamp);
    io.sockets.emit('generate target bullet', bullet);
  },
  OctetCircleBullet: function() {
    var bullet = {
      speed: 0.03,
      radius: 200,
      x: world.canvasWidth / 2 - 200,
      y: world.canvasHeight / 2 - 200,
      timestamp: (new Date).getTime()
    };
    console.log('Create octet circle bullet: ', bullet.timestamp);
    io.sockets.emit('generate octet circle bullet', bullet);
  }
};



function generateRandomWallBulletLocation() {
  var x = (Math.random() * 10000) % world.canvasWidth;
  var y = (Math.random() * 10000) % world.canvasHeight;
  var left = x;
  var top = y;
  var c = parseInt((Math.random() * 10) %  4);
  switch(c) {
    case 0: // start from left wall
      left = 0;
      break;
    case 1: // start from top wall
      top = 0;
      break;
    case 2: // start from right wall
      left = world.canvasWidth;
      break;
    case 3: // start from bottom wall
      top = world.canvasHeight;
      break;
  }
  return { left: parseInt(left), top: parseInt(top) };
}
