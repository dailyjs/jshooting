var http = require('http'), io = require('socket.io'),

server = http.createServer(function(req, res) { 
	var headers = {};
    headers["Content-Type"] = "text/plain";
    headers["Access-Control-Allow-Origin"] = "*";
    headers["Access-Control-Allow-Methods"] = "POST, GET, PUT, DELETE, OPTIONS";
    headers["Access-Control-Allow-Credentials"] = false;
    headers["Access-Control-Max-Age"] = '86400'; // 24 hours
    headers["Access-Control-Allow-Headers"] = "X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept";
    
    res.writeHead(200, headers); 
	res.write('<h1>Sample server created with NodeJS.</h1>');     
	res.end(); 
}); 
server.listen(8092);   

var socket = io.listen(server, { origins: '*:*' }); 
var game = null, players = [];
var ingp = { red: {}, blue: {} };

var Game = function() {
	var MAP_SIZE = 500, GAME_DELAY = 3000, BALL_DELAY = 1500, BALL_COUNT = 6;
	var timer = null;
	
	function init() {
		setTimeout(function() {
			timer = setInterval(function() {
				var balls = getBalls();
				
				ingp.red.client.emit("createBall", balls);
				ingp.blue.client.emit("createBall", balls);
			}, BALL_DELAY);
		}, GAME_DELAY);
	}
	
	function getBalls() {
		var balls = [];
		
		for(var i = 0; i < BALL_COUNT; i++) {
			balls.push({
				color: "#" + ((1 << 24) * Math.random() | 128).toString(16),
				speed: getRnd(5, 3),
				ball_size: getRnd(10, 5),
				p1: { x: getRnd(MAP_SIZE), y: 0 },
				p2: { x: getRnd(MAP_SIZE), y: MAP_SIZE }
			});
		}
		
		return balls;
	}

	function getRnd(max, min) {
		return Math.floor(Math.random() * max) + ((min > 0) ? min : 1);
	}
	
	this.destroy = function() {
		clearInterval(timer);
	}
	
	init();
}

socket.on('connection', function(client) {
	players.push(client);
	
	if(ingp.red.client && ingp.blue.client) {
		client.emit("limit");
	} else {
		client.emit("join");
		
		if(!ingp.red.client && players.length > 0) {
			ingp.red.client = players.shift();
		}
		
		if(!ingp.blue.client && players.length > 0) {
			ingp.blue.client = players.shift();
		}
		
		if(ingp.red.client && ingp.blue.client) {
			if(!game) {
				ingp.red.client.emit("play", { color: "red" });
				ingp.blue.client.emit("play", { color: "blue" });
				game = new Game();
			}
		}
	}
	
	client.on("move", function(data) {
		var key = (data.color == "red") ? "blue" : "red";
		
		ingp[key].client.emit("moveEnemy", data.pos);
		ingp[key].pos = data.pos;
	});

	client.on('disconnect', function() {
		if(ingp.red.client == client) { 
			ingp.red.client = null;
			ingp.blue.client.emit("outEnemy");
		}
		
		if(ingp.blue.client == client) { 
			ingp.blue.client = null;
			ingp.red.client.emit("outEnemy");
		}
		
		if(game) {
			game.destroy();
			game = null;
		}
    });
}); 