var http = require('http'), io = require('socket.io'),

server = http.createServer(function(req, res) { 
	var headers = {};
    headers["Content-Type"] = "text/plain";
    headers["Access-Control-Allow-Origin"] = "*";
    headers["Access-Control-Allow-Methods"] = "POST, GET, PUT, DELETE, OPTIONS";
    headers["Access-Control-Allow-Credentials"] = false;
    headers["Access-Control-Max-Age"] = '86400'; // 24 hours
    headers["Access-Control-Allow-Headers"] = "X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept";
    
    res.writeHead(200, headers); 
	res.write('<h1>Sample server created with NodeJS.</h1>');     
	res.end(); 
}); 
server.listen(8092);   

var Game = function(user, targetUser) {
	var MAP_SIZE = 500, GAME_DELAY = 5000, BALL_DELAY = 2000, BALL_COUNT = 10;
	var timer = null, ball_no = 0;
	
	function getBalls() {
		var balls = [];
		
		for(var i = 0; i < BALL_COUNT; i++) {
			ball_no += 1;
			
			balls.push({
				id: ball_no,
				color: "#" + ((1 << 24) * Math.random() | 128).toString(16),
				speed: getRnd(5, 5),
				ball_size: getRnd(10, 5),
				p1: { x: getRnd(MAP_SIZE), y: 0 },
				p2: { x: getRnd(MAP_SIZE), y: MAP_SIZE }
			});
		}
		
		return balls;
	}

	function getRnd(max, min) {
		return Math.floor(Math.random() * max) + ((min > 0) ? min : 1);
	}
	
	this.run = function() {
		setTimeout(function() {
			timer = setInterval(function() {
				var balls = getBalls();
				
				user.createBall(balls);
				targetUser.createBall(balls);
			}, BALL_DELAY);
		}, GAME_DELAY);
	}
	
	this.out = function() {
		this.clear();
		
		if(user) user.outEnemy();
		if(targetUser) targetUser.outEnemy();
	}

	this.clear = function() {
		if(timer) clearInterval(timer);
	}
	
	this.inUser = function(checkUser) {
		if(user != checkUser && targetUser != checkUser) return false;
		return true;
	}
}

var User = function(socket, nick) {
	var self = this;
	var enemy = null;
	var status = 0; // 0:대기, 1:대상찾기, 2:플레이
	
	function init() {
		socket.on("move", function(data) {
			if(status == 2) {
				enemy.getSocket().emit("moveEnemy", data);
			}
		});
		
		socket.on("removeBall", function(id) {
			if(status == 2) {
				enemy.getSocket().emit("removeBall", id);
			}
		});
	}
	
	this.search = function(count) {
		if(status == 1) {
			socket.emit("search", count);
		}
	}
	
	this.play = function(myColor, enemyUser) {
		if(status == 1) {
			enemy = enemyUser;
			socket.emit("play", { color: myColor, enemyNick: enemy.getNick() });
			status = 2;
		}
	}
	
	this.defeat = function() {
		if(status == 2) {
			socket.emit("defeat", { enemyNick: enemy.getNick() });
			status = 0;
			
			enemy.victory();
		}
	}
	
	this.victory = function() {
		if(status == 2) {
			socket.emit("victory", { enemyNick: enemy.getNick() });
			status = 0;
		}
	}
	
	this.outEnemy = function() { // 게임 도중 나감
		if(status == 2) {
			socket.emit("outEnemy");
			status = 0;
		}
	}
	
	this.createBall = function(balls) {
		if(status == 2) {
			socket.emit("createBall", balls);
		}
	}
	
	this.setStatus = function(s) {
		status = s;
	}
	
	this.getStatus = function() {
		return status;
	}

	this.getNick = function() {
		return nick;
	}
	
	this.getSocket = function() {
		return socket;
	}
	
	init();
}

var UserPool = function(socket) {
	var self = this;
	var games = [], users = [], user_ids = {};
	
	//
	function getUserIndex(socket) {
		for(var i = 0; i < users.length; i++) {
			if(users[i].getSocket() == socket) {
				return i;
			}
		}
		
		return null;
	}
	
	function getGameIndex(user) {
		for(var i = 0; i < games.length; i++) {
			if(games[i].inUser(user)) {
				return i;
			}
		}
		
		return null;
	}
	
	// Public Methods
	this.addUser = function(socket, nick) {
		var user = new User(socket, nick)
		
		users.push(user);
		user_ids[socket.id] = user;
		
		socket.emit("join");
	}
	
	this.removeUser = function(socket) {
		var index = getUserIndex(socket),
			user = this.getUser(socket);
		var gameIndex = getGameIndex(user),
			game = games[gameIndex];
		
		if(game) {
			// 이탈 유저가 속한 게임 삭제
			game.out();
			games[gameIndex] = null;
			games.splice(gameIndex, 1);
		}
		
		// 유저 정보 삭제
		delete user_ids[users[index].id];
		users[index] = null;
		users.splice(index, 1);
	}
	
	this.defeatUser = function(socket) {
		var index = getUserIndex(socket),
			user = this.getUser(socket);
		var gameIndex = getGameIndex(user),
			game = games[gameIndex];
		
		if(game) {
			// 패배 유저가 속한 게임 삭제
			game.clear();
			games[gameIndex] = null;
			games.splice(gameIndex, 1);

			// 유저 게임 종료
			user.defeat();
		}
	}
	
	this.getUser = function(socket) {
		return user_ids[socket.id];
	}
	
	this.searchUser = function(socket, count) {
		var user = this.getUser(socket);
		
		if(user.getStatus() == 0) {
			user.setStatus(1);
		}
		
		for(var i = 0; i < users.length; i++) {
			(function(targetUser) {
				if(user != targetUser && targetUser.getStatus() == 1) {
					self.addGame(user, targetUser);
					return;
				}
			})(users[i]);
		}
		
		if(user.getStatus() == 1) {
			setTimeout(function() {
				self.searchUser(socket, count + 1);
			}, 5000);
		}
		
		user.search(count);
	}
	
	this.addGame = function(user, targetUser) {
		var game = new Game(user, targetUser);
		
		user.play("red", targetUser);
		targetUser.play("blue", user);

		game.run();
		games.push(game);
	}
	
	this.updateCu = function() {
		for(var i = 0; i < users.length; i++) {
			users[i].getSocket().emit("cu", users.length);
		}
	}
	
	// Socket Handler
	socket.on('connection', function(client) {
		client.on("login", function(data) {
			self.addUser(client, data);
			self.updateCu();
		});
		
		client.on("search", function() {
			self.searchUser(client, 1);
		});
		
		client.on("defeat", function() {
			self.defeatUser(client);
		});
		
		client.on('disconnect', function() {
			self.removeUser(client);
			self.updateCu();
	    });
	}); 
}

new UserPool(io.listen(server, { origins: '*:*' })); 